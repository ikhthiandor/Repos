package com.android.ikhthiandor.repos.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.ikhthiandor.repos.R;
import com.android.ikhthiandor.repos.model.Repo;

import java.util.ArrayList;

/**
 * Created by ikhthiandor on 7/11/15.
 */
public class RepoRecyclerViewAdapter extends RecyclerView.Adapter<RepoRecyclerViewAdapter.ViewHolder> {


    private ArrayList<Repo> repos;

    public RepoRecyclerViewAdapter(ArrayList<Repo> repos) {
        this.repos = repos;

    }

    @Override
    public RepoRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repo, parent, false);
        return new RepoRecyclerViewAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RepoRecyclerViewAdapter.ViewHolder holder, int position) {
        Repo repo = repos.get(position);
        holder.tvRepoName.setText(repo.getName());


    }

    @Override
    public int getItemCount() {
        return repos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected TextView tvRepoName;

        public ViewHolder(View itemView) {
            super(itemView);
            tvRepoName = (TextView) itemView.findViewById(R.id.tvRepoName);
        }
    }
}
