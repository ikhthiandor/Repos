package com.android.ikhthiandor.repos.api;


import com.android.ikhthiandor.repos.model.Repo;
import com.android.ikhthiandor.repos.model.User;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;

public class ApiClient {
    private static GithubApiInterface githubService;



    public static GithubApiInterface getGithubApiClient() {

        if (githubService == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setEndpoint("https://api.github.com")
                    .build();

            githubService = restAdapter.create(GithubApiInterface.class);
        }

        return githubService;
    }

    public interface GithubApiInterface {
        @GET("/users/{user}/repos")
        void listRepos(@Path("user") String user, Callback<List<Repo>> cb);

        @GET("/users/{user}")
        void describeUser(@Path("user") String user, Callback<User> cb);
    }
}