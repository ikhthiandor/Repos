package com.android.ikhthiandor.repos.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.ikhthiandor.repos.R;
import com.android.ikhthiandor.repos.adapters.RepoRecyclerViewAdapter;
import com.android.ikhthiandor.repos.api.ApiClient;
import com.android.ikhthiandor.repos.model.Repo;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void onClickBtn(View v) {


        final EditText etUsername = (EditText) findViewById(R.id.editTextUsername);
        final String userName = String.valueOf(etUsername.getText());
        Toast.makeText(this, "Listing Repos for " + userName + "...Wait", Toast.LENGTH_LONG).show();
        ApiClient.getGithubApiClient().listRepos(userName, new Callback<List<Repo>>() {
            @Override
            public void success(List<Repo> repoList, Response response) {


                RecyclerView rvRepos = (RecyclerView) findViewById(R.id.rvRepos);
                RepoRecyclerViewAdapter rrvAdapter = new RepoRecyclerViewAdapter((ArrayList<Repo>) repoList);
                rvRepos.setAdapter(rrvAdapter);

                LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                rvRepos.setLayoutManager(llm);



            }


            @Override
            public void failure(RetrofitError error) {
                // you should handle errors, too
                if (error == null) {
                    Toast.makeText(getApplicationContext(), "error is null", Toast.LENGTH_SHORT).show();
                } else {

                    Toast.makeText(getApplicationContext(), "error is not null", Toast.LENGTH_SHORT).show();
                }

            }


        });
    }
}